#!/usr/bin/python
from math import sin, pi
from data import data
import pyaudio

def play(buff):
    p = pyaudio.PyAudio()
    stream = p.open(format = p.get_format_from_width(1), channels = 1, rate = 22050, output = True)
    for key in sorted(buff.iterkeys()):
        stream.write(chr(buff[key]))

    stream.stop_stream()
    stream.close()
    p.terminate()

def rect(x):
    x = x - int(x / (2.* pi)) * (2.* pi)
    if x < pi: return -1.;
    return 1;

def sam():
    buff = {}
    phase1 = 0.0;
    phase2 = 0.0;
    phase3 = 0.0;
    pos = 0;
    for i in xrange(0x10000):
        buff[i] = 128

    for i in xrange(166):
        amplitude1 = float(data[i*8 + 1])
        amplitude2 = float(data[i*8 + 2])
        amplitude3 = float(data[i*8 + 3])
        frequency1 = float(data[i*8 + 4]) / float(data[i*8 + 7])
        frequency2 = float(data[i*8 + 5]) / float(data[i*8 + 7])
        frequency3 = float(data[i*8 + 6]) / float(data[i*8 + 7])
        for j in xrange(250):
            phase1 += frequency1 * 0.007 * 60.;
            phase2 += frequency2 * 0.007 * 60.;
            phase3 += frequency3 * 0.007 * 60.;

            amplitude = float(amplitude1 * sin(phase1) + amplitude2 * sin(phase1) + amplitude3 * rect(phase3))
            if pos % 200 == 0:
                phase1 = 0.0;
                phase2 = 0.0;
                phase3 = 0.0;

            buff[pos] = int(amplitude*0.5+128)
            pos += 1
    return buff

play(sam())
