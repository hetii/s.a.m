#!/usr/bin/env python  

import pyaudio  
import wave, time
from z import a as kdata

#define stream chunk   
chunk = 1#24  

#open a wav format music  
f = wave.open(r"result.wav","rb")  
#instantiate PyAudio  
p = pyaudio.PyAudio()  
#open stream  
stream = p.open(format = p.get_format_from_width(f.getsampwidth()),  
                channels = f.getnchannels(),  
                rate = f.getframerate(),  
                output = True)  
#read data  
data = f.readframes(chunk)  
#paly stream  

#while data != '':  
#   stream.write(data)  
#   data = f.readframes(chunk)  

for key in sorted(kdata.iterkeys()):
#     val =  "\\x%02x" % kdata[key]
     val = chr(kdata[key])
     stream.write(val)
#     time.sleep(0.000001)

#stop stream  
stream.stop_stream()  
stream.close()  

#close PyAudio  
p.terminate() 
